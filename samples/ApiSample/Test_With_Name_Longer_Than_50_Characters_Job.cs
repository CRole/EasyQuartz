﻿//using EasyQuartz;
//using Microsoft.Extensions.Configuration;
//using Quartz;
//using System;
//using System.Threading.Tasks;
//using EasyQuartz.Storage;

//namespace ApiSample
//{
//    public class Test_With_A_Name_Longer_Than_Fifty_Characters_Job : EasyQuartzJob
//    {
//        private readonly IConfiguration _configuration;

//        public Test_With_A_Name_Longer_Than_Fifty_Characters_Job(IConfiguration configuration, IEasyQuartzJobStore easyQuartzJobStore) : base(easyQuartzJobStore)
//        {
//            _configuration = configuration;
//        }

//        public override string Cron => "0/10 * * * * ? *";

//        public override Task Execute()
//        {
//            Console.WriteLine($"[{DateTime.Now}] 我是  Test_With_A_Name_Longer_Than_Fifty_Characters_Job");
//            return Task.CompletedTask;
//        }
//    }
//}
